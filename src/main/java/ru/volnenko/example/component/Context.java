package ru.volnenko.example.component;

import ru.volnenko.example.api.IProjectService;
import ru.volnenko.example.api.ITaskService;
import ru.volnenko.example.endpoint.CalculatorEndpoint;
import ru.volnenko.example.endpoint.ProjectEndpoint;
import ru.volnenko.example.endpoint.TaskEndpoint;
import ru.volnenko.example.service.ProjectService;
import ru.volnenko.example.service.TaskService;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.ws.Endpoint;

public final class Context {

//    private final String PU = "MYSQL";
    private final String PU = "POSTGRES";

    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory(PU);

    private final IProjectService projectService = new ProjectService(emf);

    private final ITaskService taskService = new TaskService(emf);

    private final TaskEndpoint taskEndpoint = new TaskEndpoint(taskService);

    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService);

    private final CalculatorEndpoint calculatorEndpoint = new CalculatorEndpoint();

    private void publish(final String url, final Object endpoint) {
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void publish() {
        publish(TaskEndpoint.URL, taskEndpoint);
        publish(ProjectEndpoint.URL, projectEndpoint);
        publish(CalculatorEndpoint.URL, calculatorEndpoint);
    }

}
