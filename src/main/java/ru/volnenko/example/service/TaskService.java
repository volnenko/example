package ru.volnenko.example.service;

import ru.volnenko.example.api.ITaskRepository;
import ru.volnenko.example.api.ITaskService;
import ru.volnenko.example.model.Task;
import ru.volnenko.example.repository.TaskRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public final class TaskService implements ITaskService {

    private final EntityManagerFactory entityManagerFactory;

    public TaskService(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    private ITaskRepository getTaskRepository(final EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    public List<Task> findAll() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return getTaskRepository(entityManager).findAll();
        } finally {
            entityManager.close();
        }
    }

    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return getTaskRepository(entityManager).findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            final Task task = getTaskRepository(entityManager).create(name);
            entityManager.getTransaction().commit();
            return task;
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }

    public Task update(final Task task) {
        if (task == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            getTaskRepository(entityManager).update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }

    public Task removeOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            final Task task = getTaskRepository(entityManager).removeOneById(id);
            entityManager.getTransaction().commit();
            return task;
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }

    public void removeAll() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            getTaskRepository(entityManager).removeAll();
            entityManager.getTransaction().commit();
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }
    
}
