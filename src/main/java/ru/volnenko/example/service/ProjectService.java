package ru.volnenko.example.service;

import ru.volnenko.example.api.IProjectRepository;
import ru.volnenko.example.api.IProjectService;
import ru.volnenko.example.model.Project;
import ru.volnenko.example.repository.ProjectRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public final class ProjectService implements IProjectService {

    private final EntityManagerFactory entityManagerFactory;

    public ProjectService(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    private IProjectRepository getProjectRepository(final EntityManager entityManager) {
        return new ProjectRepository(entityManager);
    }

    public List<Project> findAll() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return getProjectRepository(entityManager).findAll();
        } finally {
            entityManager.close();
        }
    }

    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return getProjectRepository(entityManager).findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            final Project project = getProjectRepository(entityManager).create(name);
            entityManager.getTransaction().commit();
            return project;
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }

    public Project update(final Project project) {
        if (project == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            getProjectRepository(entityManager).update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }

    public Project removeOneById(final String id) {
        if (id == null || id.isEmpty()) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            final Project project = getProjectRepository(entityManager).removeOneById(id);
            entityManager.getTransaction().commit();
            return project;
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }

    public void removeAll() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            getProjectRepository(entityManager).removeAll();
            entityManager.getTransaction().commit();
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            entityManager.close();
        }
    }

}
