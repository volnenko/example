package ru.volnenko.example.api;

import ru.volnenko.example.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project findOneById(String id);

    Project create(String name);

    Project update(Project project);

    Project removeOneById(String id);

    void removeAll();

}
