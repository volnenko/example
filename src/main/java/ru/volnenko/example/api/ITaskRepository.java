
package ru.volnenko.example.api;

import ru.volnenko.example.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task findOneById(String id);

    Task create(String name);

    Task update(Task project);

    Task removeOneById(String id);

    void removeAll();

}
