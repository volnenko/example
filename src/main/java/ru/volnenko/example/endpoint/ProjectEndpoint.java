package ru.volnenko.example.endpoint;

import ru.volnenko.example.api.IProjectService;
import ru.volnenko.example.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint {

    public static final String URL = "http://0.0.0.0:8080/ProjectEndpoint?wsdl";

    private IProjectService projectService;

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @WebMethod
    public List<Project> findAllProject() {
        return projectService.findAll();
    }

    @WebMethod
    public Project findOneByIdProject(@WebParam(name = "id") String id) {
        return projectService.findOneById(id);
    }

    @WebMethod
    public Project createProject(@WebParam(name = "name") String name) {
        return projectService.create(name);
    }

    @WebMethod
    public Project updateProject(@WebParam(name = "project") Project project) {
        return projectService.update(project);
    }

    @WebMethod
    public Project removeOneByIdProject(@WebParam(name = "id")String id) {
        return projectService.removeOneById(id);
    }

    @WebMethod
    public void removeAllProject() {
        projectService.removeAll();
    }

}
