package ru.volnenko.example.endpoint;

import ru.volnenko.example.api.ITaskService;
import ru.volnenko.example.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint {

    public static final String URL = "http://0.0.0.0:8080/TaskEndpoint?wsdl";

    private ITaskService taskService;

    public TaskEndpoint() {
    }

    public TaskEndpoint(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @WebMethod
    public List<Task> findAllTask() {
        return taskService.findAll();
    }

    @WebMethod
    public Task findOneByIdTask(@WebParam(name = "id") String id) {
        return taskService.findOneById(id);
    }

    @WebMethod
    public Task createTask(@WebParam(name = "name") String name) {
        return taskService.create(name);
    }

    @WebMethod
    public Task updateTask(@WebParam(name = "task") Task task) {
        return taskService.update(task);
    }

    @WebMethod
    public Task removeOneByIdTask(@WebParam(name = "id")String id) {
        return taskService.removeOneById(id);
    }

    @WebMethod
    public void removeAllTask() {
        taskService.removeAll();
    }
    
}
