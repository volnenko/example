package ru.volnenko.example;

import ru.volnenko.example.component.Context;

public class Application {

    public static void main(String[] args) {
        final Context context = new Context();
        context.publish();
    }

}
