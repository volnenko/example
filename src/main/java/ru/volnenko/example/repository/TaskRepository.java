package ru.volnenko.example.repository;

import ru.volnenko.example.api.ITaskRepository;
import ru.volnenko.example.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final EntityManager entityManager;

    public TaskRepository(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Task> findAll() {
        return entityManager
                .createQuery("SELECT t FROM Task t", Task.class)
                .getResultList();
    }

    @Override
    public Task findOneById(final String id) {
        return entityManager.find(Task.class, id);
    }

    @Override
    public Task create(final String name) {
        final Task task = new Task(name);
        entityManager.persist(task);
        return task;
    }

    @Override
    public Task update(final Task task) {
        return entityManager.merge(task);
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        entityManager.remove(task);
        return task;
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Task").executeUpdate();
    }
    
}
