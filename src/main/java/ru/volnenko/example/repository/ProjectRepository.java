package ru.volnenko.example.repository;

import ru.volnenko.example.api.IProjectRepository;
import ru.volnenko.example.model.Project;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final EntityManager entityManager;

    public ProjectRepository(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Project> findAll() {
        return entityManager
                .createQuery("SELECT p FROM Project p", Project.class)
                .getResultList();
    }

    @Override
    public Project findOneById(final String id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    public Project create(final String name) {
        final Project project = new Project(name);
        entityManager.persist(project);
        return project;
    }

    @Override
    public Project update(final Project project) {
        return entityManager.merge(project);
    }

    @Override
    public Project removeOneById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return null;
        entityManager.remove(project);
        return project;
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM Project").executeUpdate();
    }

}
