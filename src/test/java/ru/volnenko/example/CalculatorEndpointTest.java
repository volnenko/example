package ru.volnenko.example;

import org.junit.Assert;
import org.junit.Test;
import ru.volnenko.example.endpoint.CalculatorEndpoint;

public class CalculatorEndpointTest {

    @Test
    public void testSum() {
        final CalculatorEndpoint calculator = new CalculatorEndpoint();
        Assert.assertEquals(3, calculator.sum(1, 2));
    }

    @Test
    public void testAbs() {
        final CalculatorEndpoint calculator = new CalculatorEndpoint();
        Assert.assertEquals(1, calculator.abs(-1));
    }

}
