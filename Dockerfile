FROM openjdk:8

ADD ./target/example.jar /opt/example.jar

EXPOSE 8080

WORKDIR /opt

ENTRYPOINT exec java -jar ./example.jar